import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class CanActiveGuardService implements CanActivate {
  constructor(private router: Router) {}
  canActivate(): boolean {
    const auth = JSON.parse(sessionStorage.getItem('userInfo') || '{}');
    if (auth !== {} && auth._id) {
      return true;
    } else {
      this.router.navigateByUrl('/login').then();
      return false;
    }
  }
}
