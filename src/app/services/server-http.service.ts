import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class ServerHttpService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // Authorization: 'my-auth-token',
    }),
  };
  private REST_API_SERVER = 'http://localhost:3200';

  constructor(private httpClient: HttpClient) {}

  public signUp(data: object) {
    const url = `${this.REST_API_SERVER}/register`;
    return this.httpClient.post<any>(url, data, this.httpOptions);
  }
  public checkUserName(username: String) {
    const url = `${this.REST_API_SERVER}/register/checkRegister?username=${username}`;
    return this.httpClient.get<any>(url, this.httpOptions);
  }
  public login(username: String, password: String) {
    const url = `${this.REST_API_SERVER}/login/checkLogin?username=${username}&password=${password}`;
    return this.httpClient.get<any>(url, this.httpOptions);
  }
  public createPost(data: FormData) {
    const url = `${this.REST_API_SERVER}/post/addpost`;
    return this.httpClient.post<any>(url, data);
  }
  public getAllPost() {
    const url = `${this.REST_API_SERVER}/post/getAllPost`;
    return this.httpClient.get<any>(url, this.httpOptions);
  }
  public addLike(data: object) {
    const url = `${this.REST_API_SERVER}/post/addLike`;
    return this.httpClient.post<any>(url, data, this.httpOptions);
  }
  public disLike(data: object) {
    const url = `${this.REST_API_SERVER}/post/disLike`;
    return this.httpClient.post<any>(url, data, this.httpOptions);
  }
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    return throwError('Something bad happened; please try again later.');
  }
}
