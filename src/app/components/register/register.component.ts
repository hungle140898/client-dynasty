import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ServerHttpService } from 'src/app/services/server-http.service';

export interface formData {
  username: string;
  email: string;
  password: string;
  phone: string;
  date: string;
  confirmpass: string;
  name: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(private router: Router, private serverHttp: ServerHttpService) {}

  public formData: formData = {
    username: '',
    email: '',
    password: '',
    phone: '',
    date: '',
    confirmpass: '',
    name: '',
  };

  public validateConfirm: boolean = false;
  public checkEmail: boolean = false;
  public checkUserName: boolean = false;

  ngOnInit(): void {
    const auth = JSON.parse(sessionStorage.getItem('userInfo') || '{}');
    if (auth !== {} && auth._id) {
      this.router.navigateByUrl('/').then();
    }
    this.checkUserRegister();
  }

  public checkUserRegister(): void {
    console.log(this.formData.username);
    // this.serverHttp.checkUserName(this.formData.username).subscribe((res) => {
    //   if (res) {
    //     if (res === 'Success') {
    //       alert('SUCCESS');
    //     } else {
    //       alert('FAILED');
    //     }
    //   }
    // });
  }

  modelChanged(event: string): any {
    if (this.formData.username !== '') {
      this.serverHttp.checkUserName(event).subscribe((res) => {
        if (res) {
          if (res === 'Success') {
            this.checkUserName = false;
          } else {
            this.checkUserName = true;
          }
          console.log(res);
        }
      });
    }
  }

  public validateEmail(formData: string): any {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(formData).toLowerCase());
  }

  register(form: NgForm): void {
    const data = this.formData;
    if (form.valid) {
      if (data.confirmpass !== data.password) {
        this.validateConfirm = true;
      } else {
        this.validateConfirm = false;
        this.serverHttp.signUp(data).subscribe((res) => {
          if (res && res === 'Success') {
            alert('Successfully');
            this.formData = {
              username: '',
              email: '',
              password: '',
              phone: '',
              date: '',
              confirmpass: '',
              name: '',
            };
            this.router.navigateByUrl('/login').then();
          } else {
            alert('Error');
          }
        });
      }
    }

    // const { email, password } = form.value;
    // if (!form.valid) {
    //   return;
    // }
    // if (email === 'admin' && password === '123123')
    //   this.router.navigateByUrl('/').then();
    // form.resetForm();
  }
}
