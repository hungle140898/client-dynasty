import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServerHttpService } from 'src/app/services/server-http.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-new-feed-post',
  templateUrl: './new-feed-post.component.html',
  styleUrls: ['./new-feed-post.component.scss'],
})
export class NewFeedPostComponent implements OnInit {
  REST_API_SERVER: string = 'http://localhost:3200/';
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private serverHttp: ServerHttpService,
    private _snackBar: MatSnackBar
  ) {}
  public posts: any = [];
  liked: boolean = false;
  userId: any = JSON.parse(sessionStorage.getItem('userInfo') || '0');
  ngOnInit(): void {
    this.getAllPost();
  }

  getAllPost(): void {
    this.posts = [];
    this.serverHttp.getAllPost().subscribe((res) => {
      if (res && res.length > 0) {
        res.forEach((item: any) => {
          item.createdAt = new Date(item.createdAt);
          const indexLiked = item.person_like.findIndex(
            (like: any) => like.userID === this.userId._id
          );
          if (indexLiked !== -1) {
            item = {
              ...item,
              liked: true,
              reactId: item.person_like[indexLiked].likeReact,
            };
          } else {
            item = {
              ...item,
              liked: false,
              reactId: 0,
            };
          }
          this.posts.push(item);
        });
      }
    });
  }
  addLike(postId: string, reactionId: number): void {
    const dislike = this.posts.some((item: any) => item.reactId === reactionId);
    if (this.userId !== 0) {
      if (dislike) {
        var reactId: string = '';
        var likedIndex = this.posts.findIndex(
          (item: any) => item._id === postId
        );
        const likedId = this.posts[likedIndex].person_like.findIndex(
          (item: any) => item.userID === this.userId._id
        );
        reactId = this.posts[likedIndex].person_like[likedId]._id;
        const objectDisLike = {
          postID: postId,
          likeID: reactId,
        };
        this.serverHttp.disLike(objectDisLike).subscribe((res) => {
          if (res && res === 'Dislike success') {
            this.getAllPost();
          }
        });
      } else {
        const objectAddLike = {
          userID: this.userId._id,
          likeReact: reactionId,
          postID: postId,
        };
        this.serverHttp.addLike(objectAddLike).subscribe((res) => {
          if (res && res === 'Liked Success') {
            this.getAllPost();
          }
        });
      }
    }
  }
}
