import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFeedPostComponent } from './new-feed-post.component';

describe('NewFeedPostComponent', () => {
  let component: NewFeedPostComponent;
  let fixture: ComponentFixture<NewFeedPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewFeedPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFeedPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
