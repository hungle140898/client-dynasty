import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServerHttpService } from 'src/app/services/server-http.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.scss'],
})
export class NewsFeedComponent implements OnInit {
  onAddMedia: boolean = false;
  newPost!: FormGroup;
  imgSrc: any;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private serverHttp: ServerHttpService,
    private _snackBar: MatSnackBar
  ) {}
  public data = [
    { name: 'Timeline', icon: 'timeline' },
    { name: 'Actibity', icon: 'people' },
    { name: 'Message', icon: 'mail_outline' },
    { name: 'Gallery', icon: 'burst_mode' },
    { name: 'Setting', icon: 'perm_data_setting' },
    { name: 'Logout', icon: 'refresh' },
  ];
  public dataSuggeted = [
    {
      img: 'https://i.imgur.com/GJrstzG.jpg',
      name: 'Susana Mendis',
      major: 'Fashion Designer',
    },
    {
      img: 'https://i.vietgiaitri.com/2020/8/18/asian-baby-girl---ve-dep-gai-hu-chau-a-thanh-xu-huong-2c3-5167934.jpg',
      name: 'Neel Litoriya',
      major: 'Cyper Punk',
    },
    {
      img: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/7716f9dd86be1b198835dd0358244a1a.jpg',
      name: 'Barbara Doe',
      major: 'Photographer',
    },
  ];
  ngOnInit(): void {
    this.newPost = this.fb.group({
      message: ['', Validators.required],
      imageLink: '',
      videoLink: '',
    });
  }
  logout(): void {
    sessionStorage.clear();
    this.router.navigateByUrl('/login').then();
  }
  addPhoto(): void {
    this.onAddMedia = !this.onAddMedia;
  }
  openSnackBarSuccess() {
    this._snackBar.open('Success !!', 'Close', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 3000,
      panelClass: ['success-alert'],
    });
  }
  openSnackBarFail() {
    this._snackBar.open('Error !!', 'Close', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 3000,
      panelClass: ['error-alert'],
    });
  }
  createPost(): void {
    const userId = JSON.parse(sessionStorage.getItem('userInfo') || '0');
    if (userId !== 0) {
      var formData = new FormData();
      formData.append('userID', userId._id);
      formData.append('imageLink', this.newPost.value.imageLink);
      formData.append('videoLink', '');
      formData.append('message', this.newPost.value.message);
      this.serverHttp.createPost(formData).subscribe((res) => {
        if (res && res === 'Success') {
          this.newPost.reset();
          this.openSnackBarSuccess();
          this.onAddMedia = false;
          var img: any = document.querySelector('#preview img');
          img.file = '';
        } else {
          this.openSnackBarFail();
        }
      });
    }
  }
  onFileInput(event: any): void {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      this.newPost.value.imageLink = file;
      var img: any = document.querySelector('#preview img');
      img.file = file;
      var reader = new FileReader();
      reader.onload = (function (aImg) {
        return function (e: any) {
          aImg.src = e.target.result;
        };
      })(img);
      reader.readAsDataURL(file);
    }
  }
}
