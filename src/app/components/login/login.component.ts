import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServerHttpService } from 'src/app/services/server-http.service';
// import {Subscription} from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  // subs: Subscription[] = [];
  loginForm!: FormGroup;
  constructor(
    // private afAuth: AngularFireAuth,
    //           private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private serverHttp: ServerHttpService
  ) {}
  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    const auth = JSON.parse(sessionStorage.getItem('userInfo') || '{}');
    if (auth !== {} && auth._id) this.router.navigateByUrl('/').then();
  }
  login(): void {
    const { username, password } = this.loginForm.value;
    this.serverHttp.login(username, password).subscribe((res) => {
      if (res && res !== 'Failed') {
        sessionStorage.setItem('userInfo', JSON.stringify(res[0]));
        this.router.navigateByUrl('/').then();
      } else {
        alert('Wrong username or password ! Please try again !');
      }
    });
  }
}
